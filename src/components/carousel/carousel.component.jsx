import React from "react";
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";

export default function Carousel() {
  return (
    <div>
      <div class="image">
        <h6 class="heading">LEVAR PESSOAS EXTRAORDINÁRIAS AO ÁPICE</h6>
        <h1 class="h1">ATRAVÉS DA ODONTOLOGIA </h1>
        <h1 class="h1">E DO AUTOCONHECIMENTO</h1>
        <p>
          <button class="btn profile-container-button">MARCAR AVALIAÇÃO</button>
        </p>
        <div>
          <InstagramIcon /> <FacebookIcon className="icon" />
        </div>
      </div>
    </div>
  );
}
