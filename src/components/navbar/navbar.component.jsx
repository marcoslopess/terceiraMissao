import React from "react";
import { Link } from "react-router-dom";

import Logo from "../../assets/logo.svg";

export default function Navbar() {
  return (
    <div>
      <nav className="navbar navbar-expand-sm fixed-top navbar-light">
        <div className="container">
          <Link className="navbar-brand" to="/dashboard">
            <img src={Logo} alt="" className="logo" />
          </Link>
          <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul className="navbar-nav mr-auto">
              <li className="navbar nav-item active">
                <Link className="navbar nav-link" to="/">
                  DR. FLÁVIO
                </Link>
              </li>
              <li className="navbar nav-item active">
                <Link className="navbar nav-link" to="/">
                  ESPECIALIDADE
                </Link>
              </li>
              <li className="navbar nav-item active">
                <Link className="navbar nav-link" to="/">
                  DEPOIMENTO
                </Link>
              </li>
              <li className="navbar nav-item active">
                <Link className="navbar nav-link" to="/">
                  MANIFESTO
                </Link>
              </li>
              <li className="navbar nav-item active">
                <Link className="navbar nav-link" to="/">
                  CONTATO
                </Link>
              </li>
            </ul>
            <button
              onClick=""
              className="profile-container-button"
              type="button"
            >
              MARCAR AVALIAÇÃO
            </button>
          </div>
        </div>
      </nav>
    </div>
  );
}
