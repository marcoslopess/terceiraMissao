import React from "react";

import Doctor from '../../assets/doctor_profile.png';

export default function Carousel() {
  return (
    <div className="container">
      <div className="row">
        <div className="col">
        {/* foto-doutor */}
          <div className="foto-doutor"><img src={Doctor} width="68%" height="30%" alt="" /> </div>
        </div>
        <div className="col">
          <div className="foto-doutor ">
            <h5>DR. FLÁVIO LIMA</h5>
            <h2>
              CARISMA E CUIDADO
              <br />
              COM OS PACIENTES{" "}
            </h2>
            <hr />
            <p>
              Filho e neto de dentistas, o Dr. Flávio sempre movido por elevar o
              potencial das pessoas. Especialista em Smile Design, enxergou na
              odontologia uma forma de agregar o empreendedorismo e o
              autoconhecimento, que para ele, são coisas que caminham lado a
              lado. Conhecido por seu elevado nível de carisma e cuidado com
              seus pacientes, sendo também reconhecido pela busca constante pelo
              desenvolvimento pessoal.
            </p>
            <h5>+ Currículo</h5>
            <br/>
            <button
              class="btn profile-container-button"
              style={{ color: "#ffffff" }}
            >
              MARCAR AVALIAÇÃO
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
