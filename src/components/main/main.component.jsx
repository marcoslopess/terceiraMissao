import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";

import "./main.styles.scss";

//NAVBAR
import Navbar from "../navbar/navbar.component";
//CAROUSEL
import Carousel from "../carousel/carousel.component";
//SOBRE O DOUTOR
import Doctor from "../doctor/doctor.component";

export default function Dashboard() {
  return (
    <div className="scrollbar scrollbar-success">
      <header>
        <Navbar />
        <Carousel />
        <Doctor />
      </header>
    </div>
  );
}
