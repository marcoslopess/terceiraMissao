import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Main from "./components/main/main.component";

export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Main} />
        <Route path="/dashboard" exact component={Main} />
      </Switch>
    </BrowserRouter>
  );
}
